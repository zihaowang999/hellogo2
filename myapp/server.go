package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	e.GET("/hello", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello")
	})
	e.GET("/world", func(c echo.Context) error {
		return c.String(http.StatusOK, "World")
	})
	e.GET("/v2/world", func(c echo.Context) error {
		return c.String(http.StatusOK, "World2")
	})
	e.GET("/error", func(c echo.Context) error {
		return c.String(http.StatusInternalServerError, "Error")
	})
	e.Logger.Fatal(e.Start(":8080"))
}
